
// Configuration parameters here...
// REST-server root URL
const API_URL = "http://localhost:8081";
const VALI_IT_AUTH_TOKEN  = "VALI_IT_AUTH_TOKEN";
const VALI_IT_AUTH_USERNAME  = "VALI_IT_AUTH_USERNAME";
const VALI_IT_AUTH_ID  = "VALI_IT_AUTH_ID";
const VALI_IT_AUTH_ISADMIN  = "VALI_IT_AUTH_ISADMIN";