function clearAuthentication() {
    localStorage.removeItem(VALI_IT_AUTH_TOKEN);
    localStorage.removeItem(VALI_IT_AUTH_USERNAME);
    localStorage.removeItem(VALI_IT_AUTH_ID);
    localStorage.removeItem(VALI_IT_AUTH_ISADMIN);
}

function storeAuthentication(loginsession) {
    localStorage.setItem(VALI_IT_AUTH_TOKEN, loginsession.token);
    localStorage.setItem(VALI_IT_AUTH_USERNAME, loginsession.username);
    localStorage.setItem(VALI_IT_AUTH_ID, loginsession.id);
    localStorage.setItem(VALI_IT_AUTH_ISADMIN, loginsession.isAdmin);
}

function getUserId() {
    return localStorage.getItem(VALI_IT_AUTH_ID);
}

function getUsername() {
    return localStorage.getItem(VALI_IT_AUTH_USERNAME);
}

function getIsAdmin() {
    return localStorage.getItem(VALI_IT_AUTH_ISADMIN);
}

function getToken() {
    return localStorage.getItem(VALI_IT_AUTH_TOKEN);
}
