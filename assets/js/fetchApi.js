

function fetchSessions() {
    return fetch(`${API_URL}/sessions`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    }
    )
        .then(checkResponse)
        .then(response => response.json());
}

function fetchUsers() {
    return fetch(`${API_URL}/users`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    }
    )
        .then(checkResponse)
        .then(response => response.json());
 }
 
  
function deleteSession(id) {
    return fetch(
        `${API_URL}/session?id=${id}`,

        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
        .then(checkResponse);
}

function deleteBooking(id) {
    return fetch(
        `${API_URL}/booking?sessionId=${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
}

function fetchIsSessionDateExpired(currentDateTime, id) {
    return fetch(
        `${API_URL}/isSessionDateExpired?date=${currentDate}?id=${id}`,
        {
            method: 'GET'
        }
    )
        .then(response => response.json());;
}

function fetchPendingBookings(id) {
    return fetch(`${API_URL}/pendingbookings?id=${id}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    }
    )
        .then(response => response.json());
}

function fetchBookings(id) {
    return fetch(`${API_URL}/bookings?id=${id}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    }
    )
        .then(response => response.json());
}

function fetchUser(id) {
    return fetch(`${API_URL}/user?id=${id}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    }
    )
        .then(checkResponse)
        .then(response => response.json());
}

function postSession(session) {
    return fetch(
        `${API_URL}/session`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(session)
        }
    )
        .then(checkResponse);
}

function updateCredit(userId, newBalance){
    let data = new FormData();
    data.append("userId", userId);
    data.append("newBalance", newBalance);
    return fetch(
        `${API_URL}/usercredit`,
        {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            },
            body: data
        }
    )
        .then(checkResponse);
 }

function postBooking(sessionId) {
    let data = new FormData();
    data.append("sessionId", sessionId);
    return fetch(
        `${API_URL}/booking`,
        {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            },
            body: data
        }
    )
        .then(checkResponse)
        .then(response => response.json());
}

function updateUser(user) {
    return fetch(
        `${API_URL}/user`,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(user)
        }
    )
        .then(checkResponse);
}

function login(credentials) {
    return fetch(
        `${API_URL}/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
        .then(checkResponse)
        .then(loginsession => loginsession.json());
}

function registerUser(user) {
    return fetch(
        `${API_URL}/register`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(user)
        }
    )
    
        .then(loginsession => loginsession.json());
}

function checkResponse(response) {
    if (!response.ok) {
        clearAuthentication();
        showLoginContainer();
        // closeSessionModal();
        generateTopMenu();
        throw new Error(response.status);
    }
    showMainContainer();
    return response;
}



