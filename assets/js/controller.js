function loadSessions() {
    fetchSessions().then(
        function (sessions) {
            let sessionsHTML = "";
            for (let i = 0; i < sessions.length; i++) {
                // if (!isSessionDateExpired) {
                if (isSessionDateExpired(sessions[i]) > 0) {
                    sessionsHTML = sessionsHTML + `
               <tr>
               <td>
               ${sessions[i].date}
               </td>
               <td>
               ${displayAddBookingButton(sessions[i])}
               </td>
               <td>
               ${displayDeleteBookingButton(sessions[i])}
               </td>
               <td>
               <button class="btn btn-primary" onclick="handleFetchBookingsButtonClick(${sessions[i].id})">Osalejaid</button>
               </td>
               <td>
               ${displayDeleteSessionButton(sessions[i])}
               </td>
               </tr>
               `;
                }
            } document.getElementById("sessionsList").innerHTML = sessionsHTML;
        }
    );
}


function isSessionDateExpired(session) {
    var today = new Date();
    today.setDate(today.getDate() - 1);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    var today = yyyy + '-' + mm + '-' + dd;
    let y = `${session.date}`;
    var n = y.localeCompare(today);
    return n;
}

function isSessionLessThanDayDue(session) {
    var today = new Date();
    today.setDate(today.getDate() + 2);
    var dd = (String(today.getDate()).padStart(2, '0'));
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    var today = yyyy + '-' + mm + '-' + dd;
    let y = `${session.date}`;
    var n = y.localeCompare(today);
    return n;
}

function displayDeleteSessionButton(session) {

    if (getIsAdmin() == "1") {
        return `<button class="btn btn-outline-primary" onclick="handleDeleteSessionButtonClick(${session.id})">Kustuta sessioon</button>`;
    } else {
        return `<button class="btn btn-outline-primary" hidden="hidden" onclick="handleDeleteSessionButtonClick(${session.id})">Kustuta sessioon</button>`;
    }
}
function displayCreditBalanceButton(user) {
    if (getIsAdmin() == "1") {

        return `<button class="btn btn-outline-success" id="CreditViewButton" onclick="handleCreditBalanceClick(${user.id})">${user.creditBalance}</button>`;
    } else {
        return `<button class="btn btn-outline-success" id="CreditViewButton"  hidden="hidden" onclick="handleCreditBalanceClick(${user.id})">${user.creditBalance}</button>`;
    }
}


function displayDeleteBookingButton(session) {
    let currentUserId = getUserId();
    let isUserBooked = false;
    let isUserPending = false;
    let isUserPendingAndThereIsSpace = false;
    for (let i = 0; i < session.bookings.length; i++) {
        if (session.bookings[i].id == currentUserId) {
            isUserBooked = true;
            break;
        }
    }
    if (isSessionLessThanDayDue(session) < 0) {
        return `<button class="btn btn-primary" onclick="handleDeleteBookingButtonClick1(${session.id})">Ei saa enam tühistada</button>`;
    } else {
        return `<button class="btn btn-primary" onclick="handleDeleteBookingButtonClick(${session.id})">Tühista broneering</button>`;
    }
}

function displayAddBookingButton(session) {
    let currentUserId = getUserId();
    let isUserBooked = false;
    let isUserPending = false;
    let isUserPendingAndThereIsSpace = false;
    for (let i = 0; i < session.bookings.length; i++) {
        if (session.bookings[i].id == currentUserId) {
            isUserBooked = true;
            break;
        }
    }
    for (let i = 0; i < session.pendingBookings.length; i++) {
        if (session.pendingBookings[i].id == currentUserId && session.bookings.length < 12) {
            isUserPendingAndThereIsSpace = true;
            break;
        }
    }
    for (let i = 0; i < session.pendingBookings.length; i++) {
        if (session.pendingBookings[i].id == currentUserId) {
            isUserPending = true;
            break;
        }
    }
    if (isUserBooked) {
        return "Tule trenni!"
    }
    else if (isUserPendingAndThereIsSpace) {
        return `<button class="btn btn-danger" id = "addBookingButton" onclick="handleAddBookingClick(${session.id})">Koht vabanes! Broneeri</button>`
    }
    else if (isUserPending) {
        return "Oled ootelistis";

    }
    else {
        return `<button class="btn btn-primary" id = "addBookingButton" onclick="handleAddBookingClick(${session.id})">Broneeri</button>`
    }
}

function loadUsers() {
    fetchUsers().then(
        function (users) {
            let usersHTML = "";

            for (let i = 0; i < users.length; i++) {
                usersHTML = usersHTML + `
               <tr>
               <td>
                   ${displayCreditBalanceButton(users[i])}
                   </td>  
               <td>
               ${"" + users[i].firstName}
               </td>
               <td>
               ${users[i].lastName + ", "}
               </td>
               <td>
                   ${users[i].eMail + ", "}
                   </td>
                   <td>
                   ${users[i].telNr}
                   </td>
                   
                   <br>
                   </td> 
               </tr>
               `;
            }
            document.getElementById("usersList").innerHTML = usersHTML;
        }
    );
}

function loadBookings(id) {
    fetchBookings(id).then(
        function (bookings) {
            let bookingsHTML = "";

            for (let i = 0; i < bookings.length; i++) {
                bookingsHTML = bookingsHTML + `
                   <tr>
                   <td>
                   ${(i + 1) + ". " + bookings[i].firstName}
                   </td>
                   <td>
                   ${bookings[i].lastName + ", "}
                   </td>
                   <td>
                   ${bookings[i].eMail + ", "}
                   </td>
                   <td>
                   ${bookings[i].telNr}
                   <br>
                   </td>            
                   </tr>
                
                    `;
            }
            document.getElementById("bookingsList").innerHTML = bookingsHTML;
        }
    );
}

function loadPendingBookings(id) {
    fetchPendingBookings(id).then(
        function (pendingBookings) {
            let pendingBookingsHTML = "";

            for (let i = 0; i < pendingBookings.length; i++) {
                pendingBookingsHTML = pendingBookingsHTML + `
                   <tr>
                   <td>
                   ${(i + 1) + ". " + pendingBookings[i].firstName}
                   </td>
                   <td>
                   ${pendingBookings[i].lastName + ", "}
                   </td>
                   <td>
                   ${pendingBookings[i].eMail + ", "}
                   </td>
                   <td>
                   ${pendingBookings[i].telNr}
                   <br>
                   </td>            
                   </tr>
                
                    `;
            }
            document.getElementById("pendingBookingsList").innerHTML = pendingBookingsHTML;
        }
    );
}
function handleDeleteSessionButtonClick(id) {
    if (confirm("Oled sa kindel?")) {
        deleteSession(id).then(
            function () {
                loadSessions();

            }
        );
    }
}
function handleDeleteBookingButtonClick1(id) {
    $("#deleteBookingRulesModal").modal("show");

}
function handleDeleteBookingButtonClick(id) {
    if (confirm("Oled sa kindel?")) {
        deleteBooking(id).then(
            function () {
                alert('broneering kustutatud');
                loadSessions();
            }
        );
    }
}

function handleFetchBookingsButtonClick(id) {
    $("#bookingsModal").modal("show");
    loadBookings(id);
    loadPendingBookings(id);
}

function handleRulesButtonClick() {
    $("#rulesModal").modal("show");
}

function handleFetchUsersButtonClick() {
    $("#usersModal").modal("show");
    loadUsers();
}


function handleAddSessionButtonClick() {
    if (getIsAdmin() == "1") {
        $("#sessionModal").modal("show");
        document.getElementById("id").value = null;
        document.getElementById("date").value = null;
    }
}

function handleUserDataUpdatingClick() {
    let currentUserId = getUserId();
    $("#userDataUpdatingModal").modal("show");
    fetchUser(currentUserId).then(
        function (user) {
            document.getElementById("updateFirstName").value = user.firstName;
            document.getElementById("updateLastName").value = user.lastName;
            document.getElementById("updateEMail").value = user.eMail;
            document.getElementById("updateTelNr").value = user.telNr;
            document.getElementById("updatePassword").value = null;
            document.getElementById("updateCreditBalance").value = user.creditBalance;

        }
    );
}

function handleUserDataUpdating() {
    let user = {
        firstName: document.getElementById("updateFirstName").value,
        lastName: document.getElementById("updateLastName").value,
        eMail: document.getElementById("updateEMail").value,
        telNr: document.getElementById("updateTelNr").value,
        password: document.getElementById("updatePassword").value
    };
    updateUser(user).then(
        function () {
            $("#userDataUpdatingModal").modal("hide");
        }
    );
}

function handleSaveSession() {
    if (document.getElementById("id").value > 0) {
        handleEditSession();
    }
    else {
        handleAddSession();
    }

}

function handleEditSession() {
    let session = {
        id: document.getElementById("id").value,
        date: document.getElementById("date").value,
    };
    postSession(session).then(
        function () {
            loadSessions();
            $("#sessionModal").modal("hide");
        }
    );
}

function handleAddSession() {
    let session = {
        id: document.getElementById("id").value,
        date: document.getElementById("date").value,
    };
    postSession(session).then(
        function () {
            loadSessions();
            $("#sessionModal").modal("hide");
        }
    );
}
function handleCreditBalanceClick(userId) {
    $("#creditBalanceUpdateModal").modal("show");
    document.getElementById("updateCreditUserId").value = userId;
    document.getElementById("updateCredit").value = "";
}

function updateCreditClick() {
    let userId = document.getElementById("updateCreditUserId").value;
    let newBalance = document.getElementById("updateCredit").value;
    console.log(document.getElementById("updateCreditUserId").value);
    updateCredit(userId, newBalance).then(
        function () {
            $("#creditBalanceUpdateModal").modal("hide");
            loadUsers();
        }
    );
}

function handleAddBookingClick(sessionId) {
    postBooking(sessionId).then(
        function (resultInfoDto) {
            console.log(resultInfoDto);
            if (resultInfoDto.status === "OK") {
                alert('bronn lisatud!');
            } else if (resultInfoDto.status == "OK2") {
                alert('saal täis: ' + resultInfoDto.message);
            }
            else if (resultInfoDto.status == "FAIL") {
                alert('Broneerimine ebaõnnestus: ' + resultInfoDto.message);
            } loadSessions();
        }
    )
}

function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (!validateCredentials(credentials)) {
        alert('Sellist kasutajat ei ole.');
    }
    else {
        login(credentials).then(loginsession => {
            storeAuthentication(loginsession);
            generateTopMenu();
            loadSessions();
            if (getIsAdmin() === "0") {
                $("#addSessionButton").hide();
            } else {
                $("#addSessionButton").show();
            }
        })

    }
}

function logoutUser() {
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
}
function handleChangePasswordButtonClick() {
    $("#passwordChangingModal").modal("show");
}

function handleRegisterUserButtonClick() {
    $("#registrationModal").modal("show");
    document.getElementById("registerUsername").value = null;
    document.getElementById("registerPassword").value = null;
    document.getElementById("registerFirstName").value = null;
    document.getElementById("registerLastName").value = null;
    document.getElementById("registerEMail").value = null;
    document.getElementById("registerTelNr").value = null;

}

function handleRegisterUser() {
    let user = {
        username: document.getElementById("registerUsername").value,
        password: document.getElementById("registerPassword").value,
        firstName: document.getElementById("registerFirstName").value,
        lastName: document.getElementById("registerLastName").value,
        eMail: document.getElementById("registerUsername").value,
        telNr: document.getElementById("registerTelNr").value,


    };
    if (isFormValid() === false) {
        alert('Kõik väljad on kohustuslikud!');
    }
    else {
        registerUser(user).then(
            function (response) {
                if (response.errors.length == 0) {
                $("#registrationModal").modal("hide");
                } else {
                    alert("Viga registreerimisel: " + response.errors[0]);
                }
            });
    }

}

// Validation
function isFormValid() {
    let username = document.getElementById("registerUsername").value;
    let password = document.getElementById("registerPassword").value;
    let firstName = document.getElementById("registerFirstName").value;
    let lastName = document.getElementById("registerLastName").value;
    let telNr = document.getElementById("registerTelNr").value;

    if (username === null || username.length < 1 || password === null || password.length < 1 ||
        firstName === null || firstName.length < 1 || lastName === null || lastName.length < 1 ||
        telNr === null || telNr.length < 1) {
        return false;
    }
    return true;
}

function handleEditButtonClick(id) {
    $("#sessionModal").modal("show");
    fetchSession(id).then(
        function (session) {
            document.getElementById("id").value = session.id;
            document.getElementById("date").value = session.date;

        }
    );
}
/* function handleAddBookingClick(sessionId) {

    fetchBookings().then(
        function (bookings) {
            let bookings1 = bookings.length
            postBooking(sessionId).then(
                fetchBookings().then(
                    function (bookings) {
                        if (bookings1 != bookings.length) {
                            alert('juhuuu');
                        }
                        if (bookings.length1 != bookings.length1) {
                            alert('broneering juba olemas');
                        }
                    }
                )
            )
        }
    )
} */

/*function handleAddBookingClick(sessionId) {
    postBooking(sessionId).then(
        function() {
            alert('juhuuu');
        }
    );

}*/
